package happyfamily;

import java.util.Arrays;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String schedule[][];
    private Family family;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    void greetPet(){
        System.out.println("Привет, "+ family.getPet().getNickname()+".");
    }

    void describePet(){
        String petTrickLevel;
        if (family.getPet().getTrickLevel()>50){
            petTrickLevel="очень хитрая";
        }
        else {
            petTrickLevel="почти не хитрая";
        }
        System.out.println("У меня есть "+family.getPet().getSpecies()+", ему "+family.getPet().getAge()+" лет, он "+petTrickLevel+".");
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName()+"{name= '"+name+"', surname= '"+surname+"', year="+year+", iq="+iq+", schedule= "+ Arrays.deepToString(schedule)+"}";
    }

    boolean feedPet(boolean timeToEat){
        Random rndm= new Random();
        int random=rndm.nextInt(101);
        boolean feeding;
        if (timeToEat){
            System.out.println("Хм... покормлю ка я "+family.getPet().getNickname()+".");
            feeding=true;
        }
        else {
            if (random<family.getPet().getTrickLevel()){
                System.out.println("Хм... покормлю ка я "+family.getPet().getNickname());
                feeding=true;

            }
            else{
                System.out.println("Думаю, "+family.getPet().getNickname()+" не голодна.");
                feeding=false;
            }
        }
        return feeding;
    }

    Human(String name, String surname, int year){

        this.name=name;
        this.surname=surname;
        this.year=year;

    }


    Human(String name, String surname, int year, int iq, String schedule[][]){

        this.name=name;
        this.surname=surname;
        this.year=year;
        this.iq=iq;
        this.schedule=schedule;

    }

    Human(){

    }

}
