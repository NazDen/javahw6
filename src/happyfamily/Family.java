package happyfamily;

import java.util.Arrays;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children;
    Pet pet;

    Family(Human mother, Human father){
        this.mother= mother;
        this.father= father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children= new Human[0];
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    private String getInfoAboutChildren() {
        String infoAboutChildren="";
        for (Human child : children) {
            infoAboutChildren = infoAboutChildren + child.toString() + ", ";
        }
        return infoAboutChildren;
    }


    @Override
    public String toString(){
        return this.getClass().getSimpleName()+"{mother= "+mother.toString()+", father= "+ father.toString()+", children= "+getInfoAboutChildren()+" pet= "+pet.toString()+"}";
    }

    public void addChild(Human child){
        Human [] temp= new Human[children.length+1];
        temp[temp.length-1]=child;
        for (int i = 0; i <children.length ; i++) {
            temp[i]=children[i];
        }
        setChildren(temp);
        child.setFamily(this);
     }

     boolean deleteChild(Human smbdy){
        boolean delete=false;
        int indexChildren=0;
        int indexTemp=0;
        Human [] temp=new Human[children.length];
        for (Human child:children) {
            if (child.getName().equals(smbdy.getName()) && child.getSurname().equals(smbdy.getSurname()) && child.getYear() == smbdy.getYear() && this.equals(smbdy.getFamily())) {
                indexChildren++;
                child.setFamily(null);
                delete = true;
            } else {
                temp[indexTemp] = children[indexChildren];
                indexTemp++;
                indexChildren++;
            }
        }
         if (temp[indexTemp] ==  null) {
             setChildren(Arrays.copyOf(temp,indexTemp));
         }else {
        setChildren(temp);}
        return delete;
     }

     int countFamily(){
        int counter= 2 + children.length;
        return counter;
     }
}
